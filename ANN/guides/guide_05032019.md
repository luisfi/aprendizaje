Aprendizaje: perceptrón
===========

Guía de la clase del 05/Marzo/2019, para el tema de *Redes Neuronales*.

1. Una neurona de una sola entrada
```
p = 2
w = 2.3
b = -3
```
  * ¿Cuál es la entrada de la función de transferencia (*n*)?

  ```
  n = wp + b = (2.3)(2) + (-3) = 4.6 - 3 = 1.6
  ```

  *  ¿Cuál es la salida de la neurona (*a*)?
  ```
  a = f(1.6), donde f(x) representa la función de transferencia que se quiera
  aplicar.
  ```

2. Para la neurona anterior calcule la salida para las siguientes funciones de
transferencia

  * Hardlim
  ```
  a = hardlim(1.6) = 1
  ```
  * Linear
  ```
  a = linear(1.6) = 1.6
  ```
  * Logsigmoid
  ```
  a = logsig(1.6) = 0.8320183851339245
  ```
3. Dada una neurona con dos entradas y los siguientes parámetros
```  
b = 1.2
w = [3 2]
p = [-5 6]
```
  Calcular la salida para las siguientes funciones:

  * Symmetrical Hardlim (hardlims)
  ```
  a = -1  si n < 0
  a = 1 si n >= 0
  ```
  ```
  n = wp + b = [3 2][-5 6]' + 1.2 = -3 + 1.2 = -1.8
  a = hardlims(-1.8) = -1
  ```
  * Saturating Linear (satlin)
  ```
  a = 0 si n < 0
  a = n si 0 <= n <= 1
  a = 1 si n > 1
  ```
  ```  
  n = wp + b = [3 2][-5 6]' + 1.2 = -3 + 1.2 = -1.8
  a = satlin(-1.8) = 0
  ```

  * Hyperbolic Tangent Sigmoid (tansig)
  ```
  a = (exp(n) - exp(-n)) / (exp(n) + exp(-n))
  ```
  ```
  n = wp + b = [3 2][-5 6]' + 1.2 = -3 + 1.2 = -1.8
  a = tansig(-1.8) = -0.946806012846
  ```


4. Una red neuronal con una sola capa, tiene 6 entradas y 2 salidas. Las salidas
serán continuas en el rango de [0,1]

  * ¿Cuántas neuronas se requieren?
  ```
  2 neuronas, una para cada salida.
  ```
  * ¿Cuál es la dimensión de la matriz de pesos?
  ```
  6x2
  2 neuronas
  6 entradas
  ```

  * ¿Qué tipo de función de transferencia sería la más adecuada?
  ```
  La función logsig ya que opera en el rango que se necesita y su
  resultado es un valor real.
  ```
