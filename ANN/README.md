Redes Neuronales
================

Aquí se presentan algunos conceptos y guías básicos para aprender el tema
de redes neuronales.

## Guías

1. [Perceptrón y funciones de transferencia](guides/guide_05032019.md)

## Algoritmos

1. [Funciones de transferencia](transfer_functions.ipynb)

  Notebook de Jupyter que ilustra las diferentes funciones de transferencia más
  utilizadas en las redes neuronales realizadas en *python 2.7*.

2. [Reconocedor de caracteres](character_recognition/)

  Ejemplo de aplicación de redes neuronales al reconocimiento de caracteres
  en imágenes.
