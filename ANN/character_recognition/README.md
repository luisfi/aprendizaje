Reconocedor de caracteres con Red Neuronal
==============================================

## Objetivo:

Utilizar las redes neuronales con el fin de reconocer digitos (0-9) o letras
(mayúsculas o minúsculas).

## Red Neuronal

Se utiliza una red neuronal de perceptrones, monocapa.

#### Entrada

Se van a utilizar como datos de entrada imágenes binarias
de tamaño de *RxC* pixeles, donde *R* es el número de renglones y *C* el
número de columnas; cada imagen será transformada a un vector
del tamaño del número total de pixeles.

#### Función de transferencia

Para la función de transferencia se tienen dos opciones:

1. `hardlim`

2. `hardlims`

Su uso dependerá de cómo sea definida la salida.

#### Salida

Será un vector de *N* salidas, donde *N* es el número de caracteres distintos
que se van a reconocer.

## Implementación

El programa se realizó con ***python 3***.

#### Dependencias

* `tkinter` >= 8.6
* `numpy` >= 1.16.2

#### Funcionamiento

Ejecutar el archivo [principal](main.py) de la siguiente
manera:

    $ python3 main.py

Debería aparece una ventana como la siguiente:

![Main](img/window-main.png)

> Ya se incluye un conjunto de entrenamiento y la configuración, archivos
> `data.csv.gz` y `mapper.json` respectivamente, para poder entrenar y
> probar la red neuronal. Funciona para los número 0, 1, 2 y 3.

Se pueden ajustar algunas configuraciones del programa. Para ver las opciones
de configuración ejecutar:

    $ python3 main.py -h

```
usage: main.py [-h] [-c Integer] [-r Integer] [-d Path] [-m Path]

Window configuration

optional arguments:
  -h, --help            show this help message and exit
  -c Integer, --columns Integer
                        Size of image columns
  -r Integer, --rows Integer
                        Size of image rows
  -d Path, --data-file Path
                        Path to csv data file
  -m Path, --mapper-file Path
                        Path to json mapper file
```

Los valores de configuración por *default* son:

* `[-r]` Filas de la image: `100`
* `[-c]` Columnas de la imagen: `75`
* `[-d]` Archivo de datos: `data.csv`
* `[-m]` Archivo de configuración de entrenamiento: `mapper.json`

La interfaz de usuario se compone de 3 partes:

1. Región de creación de ejemplos

    ![Matrix](img/window-matrix.png)

    Contiene una cuadro de dibujo de pixeles *RxC* que representa la imagen. Esta
    región permite crear los datos de ejemplo para poder entrenar
    la red neuronal.

    Para dibujar, con el mouse se debe dar ***click derecho*** y mantener
    pulsado, de esta manera al arrastrar el puntero se irá
    trazando la trayectoria generada. Se puede eliminar parte de la trayectoria
    utilizando el ***click izquierdo***, arrastrando de la misma manera que se
    dibujó.  Una vez que se ha dibujado el patrón se
    debe poner su valor en la casilla al lado de la etiqueta `Digito:`, por
    ejemplo:

    ![Pattern](img/window-pattern-one.png)

    Una vez que se tiene el patrón dibujado y el digito escrito, dar click en
    el botón `Guardar`. Ésta acción agregará el nuevo ejemplo en el archivo
    de datos, por *default* `data.csv`.

    Para poder limpiar el área de dibujo se puede presionar el botón `Limpiar`,
    dejando la región en blanco nuevamente.

    En la etiqueta `Número de datos:` se muestra el tamaño del conjunto de
    datos que se tiene guardado.

    La opción `Eliminar ultimo ejemplo` elimina el ultimo ejemplo
    almacenado, por si existió un error en el patrón
    dibujado o la etiqueta que representa y se pueda corregir.

    Se añade un archivo de datos de [ejemplo](data.csv.gz) para los digitos
    `0, 1, 2`  y `3`.

    > El archivo es de tipo `csv` pero se comprime usando gzip, extensión `gz`,
    > para disminuir su tamaño al crecer la cantidad de ejemplos almacenados.

    Es importante tener en cuenta que la modificación del archivo de datos
    en el disco sólo se realiza al terminar el programa. Se presenta la opción
    de poder desechar los cambios o de guardarlos, de la siguiente manera:

    ![Pattern](img/window-save-data.png)

    Al darle click en `Ok` se tomarán los nuevos datos generados y se
    reescribirá el archivo en disco, en caso contrario el archivo de datos
    se mantendrá sin cambios.

2. Región de entrenamiento

    ![Matrix](img/window-parameters.png)

    En esta región se encuentran los parámetros de entrenamiento de la red
    neuronal.

    * `Epochs`:

        Máximo número de ciclos, si la red no converge antes de este valor
        el entrenamiento finalizará. Valor por *default* `1000`.

    * `Umbral`:

        Valor mínimo del error que permite finalizar el entrenamiento. Valor
        por *defualt* `0.001`.

    * `Función`:

        Función de transferencia que se va a utilizar. Es importante tener claro
        el tipo de función a utilizar ya que el archivo de configuración de
        entrenamiento deberá respetar los valores que genera esta función.
        Valor por *defualt* `hardlim`.

    Para entrenar la red neuronal se debe tener el archivo de datos, por
    *default* `data.csv` y el de configuración de entrenamiento, por *default*
    `mapper.json`; éste último es para decirle qué caracteres o digitos
    debe reconocer la red neuronal, y debe tener la siguiente estructura:

            {
                    "digito/caracter":["vector asignado"]
            }

    deberá utilizar valores del [rango](https://es.wikipedia.org/wiki/Rango_(matem%C3%A1ticas))
    de la función de transferencia; por ejemplo, si se utiliza `hardlim`:

            {
                    "a":["0", "0"],
                    "b":["0", "1"],
                    "c":["1", "0"],
                    "d":["1", "1"],
            }

    con `hardlims`:

        {
                "a":["-1", "-1"],
                "b":["-1", "1"],
                "c":["1", "-1"],
                "d":["1", "1"],
        }

    De esta manera se permitirá que sólo se tomen tomen los vectores que representan
    a los digitos `a, b, c` y `d` del archivo de datos, para entrenar.
    De esta forma se pueden tener muchos patrones de entrenamiento y solo seleccionar
    un subconjunto de estos.

    Se agrega un archivo de configuración de entrenamiento de [ejemplo](mapper.json), para los digitos `0, 1, 2`  y `3`.

3. Región de prueba

    ![Prueba](img/window-test-region.png)

    >Para poder habilitar el botón de prueba, primero se debe entrenar la red neuronal.

    Al darle click en el botón `Probar` se tomará el patrón marcado en el
    área de dibujo y se pasará por la red neuronal para tratar de
    clasificarlo correctamente. En la etiqueta `Resultado:` aparecerá el
    digito/caracter calculado, por ejemplo:

    ![Prueba](img/window-test.png)

    > Para que funcione correctamente la imagen de prueba tiene que tener las mismas
    > dimensiones de las  imágenes  con las que fue entrenada la red neuronal.

### ToDo

* Editar archivos de datos y configuración en UI

* Mostrar si existe algún error en UI
