#! /usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Entrada pricipal para el programa.
#
# Autor:
#       Oropeza Vilchis Luis Alberto
#


import argparse
from src.gui import MainWindow
from src.controller import Controller


if __name__ == '__main__':
    # Command line arguments manager
    parser = argparse.ArgumentParser(description='Window configuration')
    parser.add_argument('-c', '--columns', metavar='Integer', type=int,
                        default=75, help='Size of image columns')
    parser.add_argument('-r', '--rows', metavar='Integer', type=int,
                         default=100, help='Size of image rows')
    parser.add_argument('-d', '--data-file', metavar='Path', type=str,
                        default='data.csv', help='Path to csv data file')
    parser.add_argument('-m', '--mapper-file', metavar='Path', type=str,
                        default='mapper.json', help='Path to json mapper file')
    args = parser.parse_args()
    # Controller creation
    controller = Controller()
    controller.create_settings(args)
    # View creation
    window = MainWindow(controller)
    window.root.mainloop()
