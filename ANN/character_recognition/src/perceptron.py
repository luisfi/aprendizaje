#! /usr/share/env python3:
# -*- coding: utf-8 -*-

# Implementación del perceptron simple, se define una 
# clase para el manejo de las funciones de transferencia 
# posibles
#
# Autor:
#        Oropeza Vilchis Luis Alberto
#


import numpy as np
import json
import csv

class Perceptron:

        def __init__(self, transfer_function,  threshold=0.001, max_epochs=1000):
                # Preformatted data (see class PreprocessorData)
                self.size_input = 0
                self.size_neurons = 0
                self.transfer_function = np.vectorize(transfer_function)  # To apply over vector elements
                # Flags to finish in case of not converging
                self.error = 1
                self.max_epochs = max_epochs
                self.threshold = threshold

        def calculate_a(self, p, w, b):
                n = np.dot(p, w.transpose()) + b
                return self.transfer_function(n)

        def calculate_error(self, t, a):
                return t - a

        def train(self, input, target):
                # Initialize dimensions
                self.dim_input = input[0].size
                self.size_neurons = target[0].size
                # Initialize weights
                self.weights = np.ones((self.size_neurons, self.dim_input))
                # Initialize bias
                self.bias = np.array(self.size_neurons)
                epochs = 0
                while self.error > self.threshold and self.max_epochs > epochs:
                        print('[INFO] Epoch {}'.format(epochs))
                        self.error = 0
                        for i in range(0, input.shape[0]):
                                sample = input[i]
                                sample = np.reshape(sample, (1,sample.size))
                                a = self.calculate_a(sample, self.weights, self.bias)
                                error = self.calculate_error(target[i], a)
                                error = np.reshape(error, (1, error.size))
                                self.weights += np.dot(error.transpose(), sample)
                                self.bias = self.bias + error
                                if sum([1 if x!=0 else 0 for x in np.nditer(error)]) != 0:
                                        self.error += 1
                        epochs += 1

        def test(self, input):
                print('[INFO] Probando ejemplo en la red neuronal')
                n = np.dot(input, self.weights.transpose()) + self.bias
                return self.transfer_function(n)

        def get_weights(self):
                return self.weights

        def get_bias(self):
                return self.bias

        def __str__(self):
                return 'Pesos: {} \nBias: {}'.format(self.weights, self.bias)


class TransferFunctions:

        def __init__(self):
            self.mapper = dict() 
            self.mapper['hardlim'] = TransferFunctions.hardlim
            self.mapper['hardlims'] = TransferFunctions.hardlims
        
        @staticmethod
        def hardlim(v):
                return 0 if v < 0 else 1

        @staticmethod
        def hardlims(v):
                return -1 if v < 0 else 1

        def get_function(self, name):
            return self.mapper[name]

        def get_all_functions(self):
            return [f for f in self.mapper.keys()]
