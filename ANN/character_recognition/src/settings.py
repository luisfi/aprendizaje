#! /usr/bin/env python3
# -*- coding: utf-8 -*-

# Clase que permite manejar las configuraciones globales para el programa
# 
# Autor:
#       Oropeza Vilchis Luis Alberto <luis.oropeza.129@gmail.com>
#


class Settings:

    def __init__(self):
        self.image_rows = 0
        self.image_columns = 0
        self.data_filename = ''
        self.mapper_filename = ''
    
    def get_image_rows(self):
        return self.image_rows
    
    def get_image_columns(self):
        return self.image_columns

    def get_data_filename(self):
        return self.data_filename

    def get_mapper_filename(self):
        return self.mapper_filename
    
    def set_image_rows(self, rows):
        self.image_rows = rows
    
    def set_image_columns(self, columns):
        self.image_columns = columns

    def set_data_filename(self, fn):
        self.data_filename = fn

    def set_mapper_filename(self, fn):
        self.mapper_filename = fn        

