#! /usr/bin/env python3
# -*- coding: utf-8 -*-

# Clase que conecta la vista con los modelos.
#
# Autor:
#   Oropeza Vilchis Luis Alberto 
#

# Models
from src.perceptron import Perceptron, TransferFunctions
from src.data import DataPreprocessor
from src.settings import Settings


class Controller:

    def __init__(self):
        self.settings = None
        self.data_manager = None
        self.neural_net = None
        self.transfer_functions = TransferFunctions()

    def create_settings(self, args):
        self.settings = Settings()
        self.settings.set_image_rows(args.rows)
        self.settings.set_image_columns(args.columns)
        self.settings.set_data_filename(args.data_file)
        self.settings.set_mapper_filename(args.mapper_file)
        self.create_data_manager()

    def create_data_manager(self):
        if self.settings:
            self.data_manager = DataPreprocessor(self.settings.get_data_filename(), self.settings.get_mapper_filename())
        else:
            print('[ALERTA] No existen las configuraciones necesarias para cargar los datos.')
     
    def train_nn(self, params):
        self.neural_net = Perceptron(self.transfer_functions.get_function(params['tf']), threshold=params['threshold'], max_epochs=params['epochs'])
        self.neural_net.train(self.data_manager.get_samples(), self.data_manager.get_targets())
 
    def test_net(self, vector):
        if self.neural_net:
            vector = self.data_manager.list2vect(vector)
            result = self.neural_net.test(vector)[0].tolist()
            return self.data_manager.mapper.map_vector(result)
        else:
            return 'Primero entrenar la red neuronal'

    def get_transfer_functions(self):
        return self.transfer_functions.get_all_functions()

    def save_sample(self, s):
        print('[INFO] Guardando ejemplo: {}'.format(s[-1]))
        self.data_manager.add_sample(s)
    
    def remove_lastest_sample(self):
        return self.data_manager.remove_lastest_sample()

    def on_exit(self):
        self.data_manager.save_data()
    
    def get_data_size(self):
        return self.data_manager.get_data_size()
