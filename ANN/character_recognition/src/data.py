#! /usr/bin/env python3
# -*- coding: utf-8 -*-

# Clase para realizar lectura y escritura de los datos para alimentar y 
# configurar a la red neuronal.
#
# Autor:
#   Oropeza Vilchis Luis Alberto
#


import numpy as np
import gzip
import csv
import json


class DataPreprocessor:

    def __init__(self, data_filename, mapper_filename):
        self.data_filename = data_filename
        self.vectors = list()
        self.targets = list()
        self.mapper = Mapper(mapper_filename)
        self.load_data()

    def load_data(self):
        data = None
        try:
            with gzip.open(self.data_filename + '.gz', 'rt') as file:
                data = csv.reader(file)
                for row in data:
                    if row[-1] in self.mapper.get_simbols():
                        self.vectors.append(list(map(int, row[:-1])))
                        self.targets.append(row[-1])
        except FileNotFoundError:
            print('[ERROR] [Lectura] No se encuentra el archivo de datos')
        except OSError:
            print('[ERROR] El archivo {} no tiene formato gzip'.format(self.data_filename))

    def save_data(self):
        try:
            with gzip.open(self.data_filename + '.gz', 'wt') as file:
                writer = csv.writer(file)
                for i in range(0, self.get_data_size()):
                    sample = self.vectors[i]
                    sample.append(self.targets[i])
                    writer.writerow(sample)
        except FileNotFoundError:
            print('[ERROR] [Escritura] No se encontró el archivo de datos')
        except OSError:
            print('[ERROR] El archivo {} no tiene formato gzip'.format(self.data_filename))
    
    def add_sample(self, s):
        self.vectors.append(s[:-1])
        self.targets.append(s[-1])

    def list2vect(self, x):
        return np.array(list(map(int, x)))

    def get_samples(self):
        return np.array(self.vectors)

    def get_targets(self):
        return np.array([self.mapper.map_simbol(x) for x in self.targets])
    
    def remove_lastest_sample(self):
        v = self.vectors.pop()
        s = self.targets.pop()
        print('[INFO] Eliminando ejemplo: {}'.format(s))
        v.append(s)
        return v
    
    def get_data_size(self):
        v_size = len(self.vectors)
        t_size = len(self.targets)
        if v_size != t_size:
            print('[ALERTA] Inconsistencia en el conjunto de entrenamiento (V:{}, T:{})'.format(v_size, t_size))
        return min([v_size, t_size])


class Mapper:

    def __init__(self, mapper_filename):
        self.mapper_filename = mapper_filename
        self.mapper = None
        self.simbols = None
        self.reverse_mapper = None
        self.load_mapper()

    def load_mapper(self):
        try:
            with open(self.mapper_filename, 'r') as json_file:
                self.mapper = json.load(json_file)
            if len(self.mapper) > 0:
                self.simbols = [x for x in self.mapper.keys()]
                self.reverse_mapper = {str(list(map(int, v))): k for k, v in self.mapper.items()}
        except FileNotFoundError:
            print('[ERROR] No se pudo abrir el archivo de configuración {}'.format(self.mapper_filename))

    def map_simbol(self, simbol):
        if self.mapper is not None:
            a = self.mapper[str(simbol)]
            return list(map(int, a))
        else:
            print('[INFO] No se encontraron valores para la configuración de mapeo')
            return None

    def map_vector(self, vector):
        try:
            return self.reverse_mapper[str(vector)]
        except KeyError:
            return 'No clasificado'

    def get_simbols(self):
        return self.simbols

