#! /usr/bin/env python3
# -*- coding: utf-8 -*-

# Se definen las vistas para la interacción con el usuario.
#
# Autor:
#   Oropeza Vilchis Luis Alberto
#


import tkinter as tk  # UI
from tkinter import messagebox
import csv


# Frame that contains pixel elements to draw pattern samples
class ImagePixels(tk.Frame):

    def __init__(self, parent, controller, position):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        self.parent = parent
        self.position = position
        self.canvas = None
        self.canvas_width = controller.settings.get_image_columns()
        self.canvas_height = controller.settings.get_image_rows()
        self.black_points = list()
        self.label_data = None
        self.label_sample = None
        self.label_num_samples = None
        self.entry_data = None
        self.button_save = None
        self.button_reset = None
        self.button_remove = None
        self.__init_ui__()

    def __init_ui__(self):
        row = self.position[0]
        column = self.position[1]
        self.canvas = tk.Canvas(width=self.canvas_width, height=self.canvas_height)
        self.canvas.grid(row=0, column=0, rowspan=6, padx=20, pady=20)
        self.canvas['bg'] = 'white'
        self.canvas.bind('<B1-Motion>', self.canvas_click_motion_mouse_left)
        self.canvas.bind('<B3-Motion>', self.canvas_click_motion_mouse_right)
        # Elements to save sample
        next_column = column + self.controller.settings.get_image_columns()
        self.label_sample = tk.Label(text='Guardar ejemplo', padx=15)
        self.label_sample.grid(row=row, column=next_column, columnspan=2)

        self.label_data = tk.Label(text='Digito:')
        self.label_data.grid(row = row + 2, column = next_column)
        self.entry_data= tk.Entry(width = 5, justify = tk.CENTER)
        self.entry_data.grid(row = row + 2, column = next_column + 1)
        self.entry_data.insert(0, '0')
        
        self.label_num_samples = tk.Label(text='Número de datos:')
        self.label_num_samples.grid(row=row + 3, column=next_column, columnspan=2)

        self.button_save = tk.Button(text='Guardar', command=self.save_sample_click)
        self.button_save.grid(row = row + 4, column = next_column)

        self.button_reset = tk.Button(text='Limpiar', command=self.reset_click)
        self.button_reset.grid(row = row + 4, column = next_column + 1)
        
        self.button_remove = tk.Button(text='Eliminar último ejemplo', command=self.remove_click)
        self.button_remove.grid(row = row + 5, column = next_column, columnspan=2, pady=10)

        self.update_num_samples()

    def canvas_click_motion_mouse_left(self, event):
        x = event.x 
        y = event.y 
        if (0 < x < self.canvas_width) and (0 < y < self.canvas_height):
            self.canvas.create_rectangle(x, y, x+1, y+1, fill='black', tags='{}-{}'.format(x,y))
            if (x,y) not in self.black_points:
                self.black_points.append((x, y))

    def canvas_click_motion_mouse_right(self, event):
        x = event.x
        y = event.y 
        if (x,y) in self.black_points:
            self.black_points.remove((x,y))
            id = self.canvas.find_withtag('{}-{}'.format(x,y))
            self.canvas.delete(id)

    def remove_click(self):
        self.controller.remove_lastest_sample()
        self.update_num_samples()

    def save_sample_click(self):
        self.controller.save_sample(self.get_vector())
        self.update_num_samples()

    def get_vector(self):
        vector = list()
        for x in range(0, self.canvas_width):
            for y in range(0, self.canvas_height):
                if (x, y) in self.black_points:
                    vector.append(1)
                else:
                    vector.append(0)
        vector.append(int(self.entry_data.get()))
        return vector

    def reset_click(self):
        self.canvas['bg'] = 'white'
        self.canvas.delete('all')
        self.black_points = list()

    def update_num_samples(self):
        self.label_num_samples['text'] = 'Número de datos: {}'.format(self.controller.get_data_size())

class NNFrame(tk.Frame):

    def __init__(self, parent,controller,  position):
        tk.Frame.__init__(self, parent)
        self.parent = parent
        self.controller = controller
        self.position = position
        self.button_start = None
        self.button_test = None
        self.label_result = None
        self.label_train = None
        self.label_test = None
        self.pixels_frame = None
        self.__init_ui__()

    def __init_ui__(self):
        row = self.position[0]
        column = self.position[1]

        # Neural net parameters
        self.label_parameters = tk.Label(text="Parámetros Red Neuronal", padx=20)
        self.label_parameters.grid(row=row, column=column, columnspan=2)

        self.label_epochs = tk.Label(text='Epochs:')
        self.label_epochs.grid(row=row + 1, column=column)
        self.entry_epochs = tk.Entry(width=9, justify=tk.CENTER)
        self.entry_epochs.grid(row=row + 1, column=column + 1)
        self.entry_epochs.insert(0, '1000')

        self.label_threshold = tk.Label(text='Umbral:')
        self.label_threshold.grid(row=row + 2, column=column)
        self.entry_threshold = tk.Entry(width=9, justify=tk.CENTER)
        self.entry_threshold.grid(row=row + 2, column=column + 1)
        self.entry_threshold.insert(0, '0.001')


        self.label_transfer_function = tk.Label(text='Función:')
        self.label_transfer_function.grid(row=row + 3, column = column)
        self.variable_menu = tk.StringVar(self.parent)
        options = self.controller.get_transfer_functions()
        self.variable_menu.set(options[0])
        self.menu_transfer_function = tk.OptionMenu(self.parent, self.variable_menu, *options)
        self.menu_transfer_function.grid(row=row + 3, column=column + 1)

        self.button_save = tk.Button(text='Entrenar', command=self.train_click)
        self.button_save.grid(row=row + 5, column=column, columnspan=2)

        # Test
        self.label_test = tk.Label(text='Probar Red', anchor=tk.CENTER, padx=10)
        self.label_test.grid(row=row, column=column + 3)
        self.button_test = tk.Button(text='Probar', command=self.test_net, state=tk.DISABLED)
        self.button_test.grid(row=row + 2, column=column + 3)
        self.label_result = tk.Label(text='Resultado: ')
        self.label_result.grid(row=row + 3, column=column + 3)


    def train_click(self):
        nn_params = dict()
        nn_params['epochs'] = int(self.entry_epochs.get())
        nn_params['threshold'] = float(self.entry_threshold.get())
        nn_params['tf'] = self.variable_menu.get()
        # nn.params['debug'] = self.
        print('[INFO] Entrenando red neuronal')
        self.button_save['state'] = tk.DISABLED
        self.controller.train_nn(nn_params)
        self.button_save['state'] = tk.NORMAL
        print('[INFO] Entrenamiento finalizado')
        self.button_test['state'] = tk.NORMAL

    def test_net(self):
        result = self.controller.test_net(self.pixels_frame.get_vector()[:-1])
        self.label_result['text'] = 'Resultado: {}'.format(result)

    def bind_pixels(self, pixels):
        self.pixels_frame = pixels

# Main view container
class MainWindow:

    def __init__(self, controller):
        self.root = tk.Tk()
        self.controller = controller
        self.configure()
        self.image = ImagePixels(self.root, controller, (0,0))
        self.net = NNFrame(self.root, controller, (0, controller.settings.get_image_columns() + 2))
        self.net.bind_pixels(self.image)

    def configure(self):
        self.root.title('Reconocedor de caracteres')
        self.root.resizable(False, False)
        self.root.protocol('WM_DELETE_WINDOW', self.on_exit)

    def on_exit(self):
        if messagebox.askokcancel('Salir', '¿Guardar cambios de los datos?'):
            self.controller.on_exit()
        self.root.destroy()
