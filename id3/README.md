ID3
===

Algoritmo desarrollado por [Ross Quinlan](https://en.wikipedia.org/wiki/Ross_Quinlan) para
generar árboles de decisiones a partir de un conjunto de datos, que permiten realizar
la clasificación de los elementos.

### Implementaciones

#### Python

Implementación básica hecha en python 2.7. Para poder utilizar se deben tener los
datos en formato **csv**, donde la última columna representa la decisión que debe tomar
el algoritmo.

Forma de uso:

	python id3.py filename

El archivo [example](example.csv) es un ejemplo de datos.

Para obtener más detalles de la evaluación del algoritmo:
 
	python id3.py filename -v

Para obtener ayuda:

	python id3.py -h
